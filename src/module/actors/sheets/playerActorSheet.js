
import BaseSheet from './baseActorSheet.js'
import ActorModel from '../actor.js'

export default class Tde5ePlayerActorSheet extends BaseSheet
{
    /** @override */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
        // Defines the .TDE5E.sheet.actor CSS space
        classes: ["TDE5E", "sheet", "actor"], 
        width: 720,
        height: 680
      });
    }

    /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   * @override
   */
    getData()
    {
        /** @type { ActorModel } */
        const sheetData = super.getData();

        // Add translated values (e.g. name to teh datamodel)
        this._addTranslatedValuesToData(sheetData.data.data);


        // Return data for rendering
        return sheetData;
    }

    /**
     * 
     * @param {Object} sheetModel 
     */
    _addTranslatedValuesToData(sheetModel)
    {
        for (const [name, attribute] of Object.entries(sheetModel.attributes || {})) 
        {
            attribute.name = game.i18n.localize("TDE5E.Attributes." + name.titleCase()); // name used for displaying label
            attribute.label = name.toLowerCase(); // name used for identifying template key
        }

        for (const [name, resource] of Object.entries(sheetModel.resources || {})) 
        {
            resource.name = game.i18n.localize("TDE5E.Core." + name.toLowerCase().titleCase()); // name used for displaying label
            resource.label = name.toLowerCase(); // name used for identifying template key
        }

        for (const [name, condition] of Object.entries(sheetModel.conditions || {})) 
        {
            condition.name = game.i18n.localize("TDE5E.Conditions." + name.toLowerCase().titleCase()); // name used for displaying label
            condition.label = name.toLowerCase(); // name used for identifying template key
        }

        for (const [name, stat] of Object.entries(sheetModel.stats || {})) 
        {
            stat.name = game.i18n.localize("TDE5E.Stats." + name.toLowerCase().titleCase()); // name used for displaying label
            stat.label = name.toLowerCase(); // name used for identifying template key
        }

        // Might not be needed, not sure yet. 
        // for (const [name, bioData] of Object.entries(sheetModel.bio || {})) 
        // {
        //     bioData.name = game.i18n.localize("TDE5E.Core." + name.titleCase()); // name used for displaying label
        //     bioData.label = name.toLowerCase(); // name used for identifying template key
        // }
    }

    /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
    activateListeners(html) 
    {
        super.activateListeners(html);

        if (!this.options.editable)
            return;
        
        // hook on skillValue input field for each skill
        $(html).find(".field-skill-value").on('submit blur', this._editSkillValue.bind(this))
        
        // Button to roll skill-check for skill
        $(html).find(".btn-skill-roll").on('click', this._rollSkill.bind(this))
        
        // Delete skill button, should only be visible to GM
        $(html).find(".btn-skill-delete").on('click', this._deleteSkill.bind(this))
    } 

    // ***********************************************************
    //                       Listener functions
    // ***********************************************************

    /**
     * 
     * @param {Event} event 
     */
    _rollSkill(event)
    {
        event.preventDefault();
        const itemid =  $(event.target).attr("ref");

        if (!itemid)
        {
            // TODO: Error handling via UI
            throw new Error(`Could not find itemid in html button!`);
        }

        this.entity.rollSkillCheck(itemid, {});
    }

    /**
     * 
     * @param {Event} event 
     */
    _editSkillValue(event)
    {
        event.preventDefault();
        const itemid = $(event.target).attr("ref");

        let item = duplicate(this.actor.getOwnedItem(itemid));
        let value = Number(event.target.value);

        if (value !== item.data.check.skillvalue)
        {
            item.data.check.skillvalue = value;
    
            this.actor.updateOwnedItem(item);
        }
    }

        /**
     * 
     * @param {Event} event 
     */
    _deleteSkill(event)
    {
        event.preventDefault();
        const itemid = $(event.target).attr("ref");
        this.actor.deleteOwnedItem(itemid);
    }
    
    // ***********************************************************
    //                          Drop events
    // ***********************************************************

/** @override 
 * @param {Event | JQuery.Event} event  An event of the dropped component
*/
    async _onDrop(event)
    {
           // Try to extract the data
        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData('text/plain'));
            if (data.type !== "Item")
                return;
        }
        catch (err)
        {
            return false;
        }

        // Case 1 - Import from a Compendium pack
        const actor = this.actor;
        if (data.pack) 
        {
            return actor.importItemFromCollection(data.pack, data.id);
        }

        // Case 2 - Data explicitly provided
        else if (data.data) 
        {
            let sameActor = data.actorId === actor._id;
                
            if (sameActor && actor.isToken)
                sameActor = data.tokenId === actor.token.id;
            if (sameActor)
                return this._onSortItem(event, data.data); // Sort existing items

            else return actor.createEmbeddedEntity("OwnedItem", duplicate(data.data));  // Create a new Item
        }

        // Case 3 - Import from World entity
        else {
            let item = game.items.get(data.id);

            if (!item)
                return;
            
            return actor.createEmbeddedEntity("OwnedItem", duplicate(item.data));
        } 
    }
}