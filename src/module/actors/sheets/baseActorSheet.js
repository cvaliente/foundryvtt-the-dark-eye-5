export default class Tde5eBaseActorSheet extends ActorSheet 
{


    /** @override */
    static get defaultOptions()
    {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    /** @override */
    get template()
    {
        if (!game.user.isGM && this.actor.limited)
            return "systems/tde5e/templates/actors/limited-player-sheet.html";
        
        return `systems/tde5e/templates/actors/${this.actor.data.type}-sheet.html`;
    }
    
    /** 
     *  Is handed down to the HTML for the corresponding sheet
     * @override */
    getData()
    {
        //const sheetData = super.getData();
        // Basic data
        let isOwner = this.entity.owner;
        let actorDataCopy = duplicate(this.actor.data);

        const data = {
            owner: isOwner,
            limited: this.entity.limited,
            options: this.options,
            editable: this.isEditable,
            cssClass: isOwner ? "editable" : "locked",

            isPlayer: this.entity.data.type === "player",
            isNPC: this.entity.data.type === "npc",
            isMonster: this.entity.data.type === 'monster',
            
            config: null,
            actor: actorDataCopy,
            items: {},
            data: actorDataCopy
        };

        return data;
    }
}