import  * as chat from '../chat.js'
import TDE5E_ROLLHELPER from './../helpers/rolls.js'
export const initialActorLoadListener = (/** @type {TdeBcActor} */actor) => { actor._loadInitialSkillsForActor()}


export default class TdeBcActor extends Actor
{

    on
    /**
      * @override
      * TODO: This becomes unnecessary after 0.7.x is released
      */
    initialize()
    {
        try
        {
            this.prepareData();
        } catch (err)
        {
            console.error(`Failed to initialize data for ${this.constructor.name} ${this.id}:`);
            console.error(err);
        }
    }    

    /** @override */
    prepareData()
    {
        super.prepareData();
        this.prepareEmbeddedEntities(),
        this._sortAndGroupItems();
        this.prepareDerivedData();
    }

    _sortAndGroupItems()
    {
        // Create quick access groups for item-types
        // Values are initially filled but should not be regenrated every time a model is prepared
        // Instead calls that modify items, should also update the filtered items 
        /** @type {Object} */
        const model = this.data;
        model.filtered = {};
        model.filtered.skills = [];
        model.filtered.spells = [];
        model.filtered.liturgychants = [];
        model.filtered.advantages = [];
        model.filtered.disadvantages = [];
        model.filtered.specialabilities = [];
        model.filtered.languagesspoken = [];
        model.filtered.languageswritten = [];
        model.filtered.meleeweapons = [];
        model.filtered.rangedweapons = [];
        model.filtered.shields = [];
        model.filtered.armours = [];

        //TODO: Other types have to be added

        model.items.forEach(item => 
        {
            switch (item.type)
            {
                case "skill": model.filtered.skills.push(item); break;
                case "combatskill": break;

                case "combatApplication": break;
                case "combatPreperation": break;

                //TODO: Filtering needs to be adjusted so that similar types are 
                case "checkedSpell": model.filtered.spells.push(item); break;
                case "preCheckedSpell": model.filtered.spells.push(item); break;
                case "extensibleCheckedSpell": model.filtered.spells.push(item); break;
                case "spellAbility": model.filtered.spells.push(item); break;
                case "boundSpellAbility": model.filtered.spells.push(item); break;

                case "specialability": model.filtered.specialabilities.push(item); break;
                case "advantage": model.filtered.advantages.push(item); break;
                case "disadvantage": model.filtered.disadvantages.push(item); break;
                case "languageSpoken": model.filtered.languagesspoken.push(item); break;
                case "languageWritten": model.filtered.languageswritten.push(item); break;

                case "meleeWeapon": model.filtered.meleeweapons.push(item); break;
                case "rangedWapon": model.filtered.rangedweapons.push(item); break;
                case "shield": model.filtered.shields.push(item); break;
                case "armour": model.filtered.armours.push(item); break;

                case "equipment": break;
                case "usable": break;
            }
        });

        // Sort skills by category, then by name
        model.filtered.skills.sort((s1, s2) =>
        {
            const c1 = s1.data.category;
            const c2 = s2.data.category;
            const n1 = s1.name;
            const n2 = s2.name;
            return (c1 === c2 ? n1.localeCompare(n2) : c1.localeCompare(c2));
        });
    }

    /** @override */
    getRollData()
    {
        const data = super.getRollData();
        return data;
    }

    /** @override */
    prepareDerivedData()
    {
        const actorData = this.data;
        const data = actorData.data;
        const flags = actorData.flags.dnd5e || {};
        const bonuses = getProperty(data, "bonuses.abilities") || {};
    }

    // ***********************************************************
    //              Handle new and deleted items
    // ***********************************************************

	/**
	 * Get an owned item by it's ID, initialized as an Item entity class
	 * @param {String} itemId	The ID of the owned item
	 * @return			An Item class instance for that owned item or null if the itemId does not exist
     * @override
	 */
    getOwnedItem(itemId)
    {
        return super.getOwnedItem(itemId);
    }   
    
	/**
	 * Create a new item owned by this Actor.
	 * @param {Object} itemData		Data for the newly owned item
	 * @param {Object} [options]	Item creation options
	 * @param options.rendeSheet	Render the Item sheet for the newly created item data
	 * @return			A Promise containing the newly created owned Item instance
     * @override
	 */
	async createOwnedItem(itemData, options)
    {
        return super.createOwnedItem(itemData, options);
    }
    
    	/**
	 * Delete an owned item by its id. This redirects its arguments to the deleteEmbeddedEntity method.
	 * @param {String} itemId	    The ID of the item to delete
	 * @param {Object} [options]	Item deletion options
	 * @return			A Promise resolving to the deleted Owned Item data
     * @override
	 */
    async deleteOwnedItem(itemId, options)
    {
        return super.deleteOwnedItem(itemId, options);
    }

    /**
	 * Update an owned item using provided new data
	 * @param {Object} itemData	    Data for the item to update
	 * @param {Object} [options]	Item update options
	 * @return			A Promise resolving to the updated Item object
     * @override
	 */
    async updateOwnedItem(itemData, options)
    {
        return super.updateOwnedItem(itemData, options);
    }


    /**
	 * Import a new owned Item from a compendium collection
	 * The imported Item is then added to the Actor as an owned item.
	 *
	 * @param {String} collection	The name of the pack from which to import
	 * @param {String} entryId		The ID of the compendium entry to import
     * @override
	 */
    importItemFromCollection(collection, entryId)
    {
        const pack = game.packs.get(collection);
        if (pack.metadata.entity !== "Item")
            return;
        
        return pack.getEntity(entryId).then(ent => 
        {
            // Check whether skill is a duplicate!
            if (ent.data.type === "skill")
            {
                const skillName = ent.data.name;
                const skill = this._getSkillByName(skillName);

                if (skill)
                {
                    // TODO: Localize entire text
                    ui.notifications.warn(`Cannot add duplicat skill ${game.i18n.localize(skillName)}!`);
                    return false;
                }
            }

          console.log(`${vtt} | Importing Item ${ent.name} from ${collection}`);
          delete ent.data._id;
          return this.createOwnedItem(ent.data);
        });
    }

    // ***********************************************************
    //                     on-functions
    // ***********************************************************

        /** 
     * @param {Object} data
     * @param {Object} options
     * @param {String} userId
     * @param {Object} context
     * @override */
    _onCreate(data, options, userId, context)
    {
        super._onCreate(data, options, userId, context);
    }

    /**
     * Depending on the actor type, some skill are considered mandatory and must be loaded 
     * when teh character is first created. 
     * This function will load the required skilsl and add them to the actor.
     */
    _loadInitialSkillsForActor()
    {
         // Load all default skills from 
         if (this.data.type === "player")
         {
             this._getAllDefaultSkillsFromCompendium().then(skills => 
             {
                 if(skills.length > 0)
                     this.createOwnedItem(skills);
             })
         }
    }

    async _getAllDefaultSkillsFromCompendium()
    {
        let skillsToUpdate = []

        const pack = game.packs.get("tde5e.skills")
        const packSkills = await pack.getData();

        for (const skill of packSkills.index) {
            
            const id = skill._id;
            const entity = await pack.getEntity(id)
                
            if (entity.data.data.isDefault)
            {
                const copy = duplicate(entity);
                delete copy.data._id;
                skillsToUpdate.push(copy);
            }
        };

        return skillsToUpdate;
    }


    // ***********************************************************
    //           Functions that the model exposes
    // ***********************************************************

    async rollSkillCheck(skillId, options = {})
    {
        // @ts-ignore
        const skill = this.data.filtered.skills.find(i => i._id === skillId);

        const skillData = skill.data;

        /** @type {chat.SkillCheckUserInputData} */
        const userInput = {
            attributeNames: skillData.check.attributes,
            characterName: this.data.name,
            skillName: skill.name,
            rolldata: {
                attributes: this._valuesForAttributeNames(skillData.check.attributes),
                botchRange: [20, 20],
                critRange: [1, 1],
                handicap: this._getTotalHandicapFromConditions(),
                rolls: TDE5E_ROLLHELPER.getRandomDiceRolls(3, 20),
                skillValue: skillData.check.skillvalue
            }
        }
        await chat.send3d20CheckToChat(userInput);
    }


    // ***********************************************************
    //           Helper functions for easy access of data
    // ***********************************************************

    _getSkillByName(name)
    {
        const model = this.data;

        // @ts-ignore
        for (const skill of model.filtered.skills) 
        {
            if (skill.name === name)
                return skill;    
        }

        return null;
    }


    _getTotalHandicapFromConditions()
    {
        let result = 0;
        for (const con of Object.values(this.data.data.conditions)) 
        {
            //TODO: Calculate effective conditions
            //result += (Math.max(con.current - con.ignoreamount, 0));
        }
        return result;
    }
    _valuesForAttributeNames(keys)
    {
        let result = [];
        for (const key of keys) {
            result.push(this._valueForAttributeName(key))
        }
        return result;
    }

    /**
     * 
     * @param {String} atrKey 
     */
    _valueForAttributeName(atrKey)
    {
        const keyElement = atrKey.split(".").pop();
        const attribute = this.data.data.attributes[keyElement.toLowerCase()];

        if (attribute)
            return attribute.current;
        else
            throw new Error(`Could not find attribute with name ${atrKey}`);
    }
}