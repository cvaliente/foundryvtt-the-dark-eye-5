
export { TDE5E_ROLLHELPER as default }


const TDE5E_ROLLHELPER = {};


    /**
     * Returns an array of length n  with roll results.
     * @param {*} n 
     * @param {*} faces 
     */
    TDE5E_ROLLHELPER.getRandomDiceRolls = function(n = 3, faces = 20)
    {
        var roll = new Die({number:n, faces:faces});
        return roll.evaluate().results.map(r => r.result);;
    }