export { SocketRequest, messageReceived };


class SocketRequest
{
    type = SOCKET_REQUEST_TYPE.UPDATE_ENTITY
    sourceUser = ""
    content = {}
    
    targetType = SOCKET_REQUEST_TARGET_TYPE.ALL
}

const SOCKET_REQUEST_TYPE = {
    UPDATE_ENTITY: 0,
}

const SOCKET_REQUEST_TARGET_TYPE = {
    ALL: 0,
    GM: 1,
    NONE_GM: 2
}

/**
 * 
 * @param {SocketRequest} data 
 */
function messageReceived(data)
{
    ui.notifications.info(JSON.parse(JSON.stringify(data)))
}