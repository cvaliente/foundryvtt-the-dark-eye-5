export { TDE5E as default }

const TDE5E = {}

/**
 * Describes the default categories an armour can be associated with.
 */
TDE5E.armourTypes = {
    "fabric": "TDE5E.Armour.Fabric",
    "leather": "TDE5E.Armour.Leather",
    "chain": "TDE5E.Armour.Chain",
    "plate" : "TDE5E.Armour.Plate"
};

TDE5E.defaultTalents = {
    "player": ["TDE5E.Skill.Bodycontrol"],
    "npc": ["TDE5E.Skill.Bodycontrol"],
    "monster" : [],
}

TDE5E.statusEffects = [
    {
        id: "burning",
        icon: "systems/tde5e/assets/burning.svg",
        label: "TDE5E.Conditions.Burning"
    }
]