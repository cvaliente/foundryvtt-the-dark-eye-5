
import { calculate3d20Check, SkillCheckInputData, rspToQl } from './3d20check.js'
import TDE5E_ROLLHELPER from './helpers/rolls.js'

/**
 * A class taht contains all required data that is needed to render a 3d20 check to chat.
 */
export class SkillCheckUserInputData
{
    /**@type {SkillCheckInputData} */ rolldata = null
    /**@type {String[]}*/ attributeNames = []
    /**@type {String}*/ characterName = ""
    /**@type {String}*/ skillName = ""
}  


/**
 * 
 * @param {SkillCheckUserInputData} userInput 
 */
export async function send3d20CheckToChat(userInput)
{
    const html = _getCheckHtml(userInput);
    
    let rollChatData = {
        content: html
      };
    
    (await ChatMessage.create(rollChatData)).setFlag("tde5e","checkInput", userInput);
}

/**
 * Called from the Hooks extension. 
 * Handles the case that an update for a ChatMessage was received.
 * The message "content" will then be updated but only if the message was a 3d20 check.
 * 
 * @param {ChatMessage} message 
 * @param {Object} data 
 * @param {User} user 
 */
export async function handleChatMessageUpdate(message, data, user)
{
    if (!message.isContentVisible)
        return;
    
    _updateMessageContentWithCheckRoll(message);
}

/**
 * 
 * @param {ChatMessage} message 
 */
function _updateMessageContentWithCheckRoll(message)
{
    let userInput = message.getFlag("tde5e", "checkInput");

    if (userInput)
    {
        // @ts-ignore
        message.data.content = _getCheckHtml(userInput)
    }
}

/**
 * 
 * @param {SkillCheckUserInputData} userInput 
 */
export function _getCheckHtml(userInput)
{
    // Redirect template code handling to already loaded template
    const template = `{{> "systems/tde5e/templates/chat/3d20-roll.html"}}`
    
    const rsp = calculate3d20Check(userInput.rolldata);
    const ql = rspToQl(rsp);


    // Prepare attribute data
    let attributeData = []
    for (let i = 0; i < userInput.attributeNames.length; i++) {
        attributeData.push({
            attributeName: userInput.attributeNames[i],
            attributeValue: userInput.rolldata.attributes[i],
            rollValue: userInput.rolldata.rolls[i],
        });
    }

    let rollData = {
        attributes: attributeData,
        remainingSkillPoints: rsp,
        skillName: userInput.skillName,
        skillValue: userInput.rolldata.skillValue,
        characterName : userInput.characterName,
        qualityLevel : ql
    };

    const templateFunction = Handlebars.compile(template);
    return templateFunction(rollData);
}
    

/**
 * 
 * @param {ChatMessage} message 
 * @param {any} html flexcol.message element
 * @param {*} data 
 */
export async function handleChatMessageRender(message, html, data)
{
    if (!message.isContentVisible)
        return;

    /**@type {SkillCheckUserInputData}*/
    let flag = message.getFlag("tde5e", "checkInput")

    if (flag)
    {
        var button = html.find("#modifyMessage")

        if (message.user.isSelf || game.user.isGM)
        {
            if (button.length > 0)
            {
                button.on('click', () => { _onClickModifyMessage(message._id); });
            } 
        }
        else
        {
            button.hide();
        }
    }
}

/**
 * 
 * @param {string} msgId 
 */
export async function _onClickModifyMessage(msgId)
{
    console.log("This is the id: " + msgId);

    let messageObj = new ChatMessage(game.data.messages.filter(m => m._id === msgId)[0]);
    console.log(JSON.parse(JSON.stringify(messageObj)))

    messageObj.data.flags
    /** @type {SkillCheckUserInputData} */
    let flagData = messageObj.getFlag("tde5e", "checkInput");

    console.log("Old rolls: " + flagData.rolldata.rolls)
    var newRolles = TDE5E_ROLLHELPER.getRandomDiceRolls(3, 20);
    flagData.rolldata.rolls = newRolles;

    console.log("New rolls: " + flagData.rolldata.rolls);
    messageObj.setFlag("tde5e", "checkInput", flagData);

    _updateMessageContentWithCheckRoll(messageObj);

    // Update does not register UNLESS diff:false, strange behaviour even ifonly content is set!
    //(await messageObj.update(messageObj.data, { diff: false }))
    // @ts-ignore
    await messageObj.update({ content: messageObj.data.content, 'flags.tde5e.checkInput': flagData }, {diff:false});

    // TODO: 
    // --get message associated with id
    // --change/update flags with random rolls, update content 
    // --Also test, if update will transfer ANY property given - DOES NOT
    // need to see what update does what
}

