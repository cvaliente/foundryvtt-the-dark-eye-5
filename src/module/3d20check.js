
export { calculate3d20Check, rspToQl, SkillCheckInputData };

class SkillCheckInputData
{
    /**@type {Number[]}*/ attributes = []
    /**@type {Number[]}*/ rolls = []
    /**@type {Number}*/ skillValue = 0
    /**@type {Number}*/ handicap = 0
    /**@type {Number[]}*/ critRange = []
    /**@type {Number[]}*/ botchRange = []

}      

// TODO: Add roll anlytics features

/**
 *  // TODO: Comment
 * 
 * @param {SkillCheckInputData} input 
 * @returns {Number} 
 */
function calculate3d20Check(input)
{
    var rsp = calculateRemainingSkillPoints(input.attributes, input.rolls, input.skillValue, input.handicap, input.critRange, input.botchRange)
    return rsp;
}
/**
 *  Converts a given rsp value into the qualityLevel.
 * 
 * @param {Number} rsp Remaining skill points
 * @returns {Number} Quality level (0 to 6)
 */
function rspToQl(rsp)
{
    return rsp <= 0 ? 0 : Math.ceil(rsp / 3);
}

/**
 *  Simulates a TDE check using the given values.The result can never be less than 0.
 *  If canBeZero is set to TRUE, the result will return 0 even on a successful check.
 *  The flags respectFumble and respectCrit both handle the respective cases and will return either 0 or talentValue.
 * 
 * @param {Number[]} attributes Three rolls that represent a 3d20 check
 * @param {Number[]} rolls Three rolls that represent a 3d20 check
 * @param {Number} skillValue TalentValue of the check
 * @param {Number} handicap The handicap for the chack
 * @param {Number[]} critRange Array wiith two values, start and end, both inclusive
 * @param {Number[]} botchRange Array wiith two values, start and end, both inclusive
 * @returns {Number} The remaining skillpoints. if the check would return 0, it will return 1 instead
 */
function calculateRemainingSkillPoints(attributes, rolls, skillValue, handicap, critRange = [1, 1], botchRange = [20, 20], canBeZero = false, respectFumble = true, respectCrit = true) 
{
    if (!attributes || !rolls || attributes.length !== rolls.length)
        throw Error(`Invalid parameters for calculateRemainingSkillPoints: either attribute or rolls are empty or the have mismatching lengths!`);

    if (respectCrit && _isCrit(rolls, critRange[0], critRange[1]))
        return Math.max(skillValue, 1);

    if (respectFumble && _isBotch(rolls, botchRange[0], botchRange[1]))
        return 0;

    let remainingSkillPoints = skillValue;

    for (let i = 0; i < attributes.length; i++) 
    {
        let attribute = attributes[i];
        let roll = rolls[i];
        
        remainingSkillPoints += Math.min((attribute - handicap) - roll, 0);
    }

    if (canBeZero && remainingSkillPoints == 0 )
        return 1;
    
    if (remainingSkillPoints < 0)
        return 0;

    return remainingSkillPoints;
}


/**
 * Returns true, if the given rolls amount to a crit in the DSA rulesystem.
 * 
 * @param {Number[]} rolls three rolls that represent a 3d20 check
 * @param {Number} critRangeStart RangeStart, default 1
 * @param {Number} critRangeEnd RangeEnd, 1 
 * @returns {Boolean} TRUE if crit
 * @private
 */
function _isCrit (rolls, critRangeStart = 1, critRangeEnd = 1)
{
    return _inRangeCount(rolls, critRangeStart, critRangeEnd);
}

    /**
 * Returns true, if the given rolls amount to a botch in the DSA rulesystem.
 * 
 * @param {Number[]} rolls three rolls that represent a 3d20 check
 * @param {Number} fumbleRangeStart RangeStart, default 20
 * @param {Number} fumbleRangeEnd RangeEnd, 20 
 * @returns {Boolean} TRUE if botch
 * @private
 */
function _isBotch (rolls, fumbleRangeStart = 20, fumbleRangeEnd = 20)
{
    return _inRangeCount(rolls, fumbleRangeStart, fumbleRangeEnd);
}

/**
 * Counts how many values are in the range of [startRange:endRange].
 * The beginning and end are both inclusive!
 * 
 * @param {Number[]} values 
 * @param {Number} startRange 
 * @param {Number} endRange 
 * @returns {Boolean} TRUE if values are in range
 * @private
 */
function _inRangeCount(values, startRange, endRange)
{
    var count = 0;

    values.forEach(r =>
        { if (r >= startRange && r <= endRange) count++; });

    return count > 1;
}
