export { preloadHandlebarsTemplates }

/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
const preloadHandlebarsTemplates = async function() {

    // Define template paths to load
    const templatePaths = [
  
     "systems/tde5e/templates/chat/3d20-roll.html" ,
      // Actor Sheet Partials
      "systems/tde5e/templates/actors/parts/actor-basic.html",
      "systems/tde5e/templates/actors/parts/actor-skills.html",
      "systems/tde5e/templates/actors/parts/actor-combat.html",
      "systems/tde5e/templates/actors/parts/actor-spells.html",
      "systems/tde5e/templates/actors/parts/actor-liturgy-chants.html",
      "systems/tde5e/templates/actors/parts/actor-notes.html",
      "systems/tde5e/templates/actors/parts/actor-inventory.html",
  
      // Item Sheet Partials
      //"systems/tde5e/templates/items/parts/item-action.html",
      //"systems/tde5e/templates/items/parts/item-activation.html",
      //"systems/tde5e/templates/items/parts/item-description.html",
      //"systems/tde5e/templates/items/parts/item-mountable.html"
    ];
  
    // Load the template parts
    return loadTemplates(templatePaths);
  };
  