/**
 * Register combat related hooks. These will be permanently active until
 *  explicitly removed.
 */
export const registerHooks = function() {
    // Hook to add a second turn for combatants with over 14 initiative
    Hooks.on("updateCombatant", function(combat, combatantData, newData, _options, _updateId) {
        if (!("initiative" in newData)) return;

        // Check if the token already has more than one combatant
        let linkedCombatants = combat.combatants.filter(c => c.tokenId === combatantData.tokenId);
        let hasSecondTurn = linkedCombatants.length > 1;

        if (newData.initiative > 14) {
            if (hasSecondTurn) return;

            // Add a second combatant for the same token
            let newCombatant = duplicate(combatantData);
            newCombatant.initiative = newData.initiative - 6;
            combat.createCombatant(newCombatant);
        } else if (hasSecondTurn) {
            // Initiative changed below 14, remove the second combatant
            combat.deleteCombatant(linkedCombatants[linkedCombatants.length - 1]._id);
        }
    });
}

/**
 * Override the default Initiative formula to customize special behaviors of the system.
 * Apply advantage, proficiency, or bonuses where appropriate
 * Apply the dexterity score as a decimal tiebreaker if requested
 * See Combat._getInitiativeFormula for more detail.
 */
export const _getInitiativeFormula = function(combatant) {
    const actor = combatant.actor;

    // TODO: change this so it makes sense
    if ( !actor ) return "0";
    const init = actor.data.data.attributes.init;
  
    let nd = 1;
    let mods = "";
    
    if (actor.getFlag("dnd5e", "halflingLucky")) mods += "r=1";
    if (actor.getFlag("dnd5e", "initiativeAdv")) {
        nd = 2;
        mods += "kh";
    }
  
    const parts = [`${nd}d20${mods}`, init.mod, (init.prof !== 0) ? init.prof : null, (init.bonus !== 0) ? init.bonus : null];
  
    // Optionally apply Dexterity tiebreaker
    
    const tiebreaker = game.settings.get("dnd5e", "initiativeDexTiebreaker");
    if (tiebreaker)
        parts.push(actor.data.data.abilities.dex.value / 100);
    
    return parts.filter(p => p !== null).join(" + ");
};
  