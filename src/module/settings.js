export { registerSystemSettings }


const registerSystemSettings = function ()
{
    game.settings.register("tde5e", "systemMigrationVersion", {
        name: "System Migration Version",
        scope: "world",
        config: false,
        type: Number,
        default: 0
      });
    
}