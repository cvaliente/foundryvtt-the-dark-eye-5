export { PERMISSION }

const PERMISSION = {}

PERMISSION.canDoX = function (/** @type {User} */ user) { return user.isGM && user.isSelf}
