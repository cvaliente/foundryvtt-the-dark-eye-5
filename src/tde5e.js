/**
 * This is your JavaScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your system, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your system
 */

// Import JavaScript modules
import * as chat from "./module/chat.js";
import * as socket from "./module/socket.js";
import * as templates from "./module/templates.js";
import * as settings from "./module/settings.js";
import * as combat from "./module/combat.js"
import * as check from './module/3d20check.js';

// Import Apps
import ItemModel from "./module/items/item.js";
//import TdebcItemSheet from "./module/items/sheets/itemSheet.js";
import ActorModel, { initialActorLoadListener } from "./module/actors/actor.js";
import PlayerActorSheet from "./module/actors/sheets/playerActorSheet.js";
import TDE5E from "./module/config.js";


/* ------------------------------------ */
/* Initialize system					*/
/* ------------------------------------ */
Hooks.once('init', async function() {
	console.log('tde5e | Initializing the "The Dark Eye - Bosparans Catacombs" System "');

	// TODO: game global 
	game.TDE5E = {
		applications: {},
		canvas: {},
		config: null,
		dice: null,
		entities: {
			ActorModel
		},
		macros: null,
		migrations: null,
		rollItemMacro: null
	};
	
	settings.registerSystemSettings();

	combat.registerHooks();

	CONFIG.Combat.initiative.formula = "1d20";
	// @ts-ignore
	//Combat.prototype._getInitiativeFormula = combat._getInitiativeFormula;


	// Assign custom classes and constants here
	// @ts-ignore
	CONFIG.Actor.entityClass = ActorModel;
	//CONFIG.Item.entityClass = ItemModel;
	CONFIG.statusEffects = TDE5E.statusEffects;

	// Register custom system settings
	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("tde5e", PlayerActorSheet, { makeDefault: true });
	//Items.unregisterSheet("core", ItemSheet);
	//Items.registerSheet("tde5e", TdebcItemSheet, { makeDefault: true });

	// Preload Handlebars templates
	templates.preloadHandlebarsTemplates();
});

/* ------------------------------------ */
/* Setup system							*/
/* ------------------------------------ */
Hooks.once('setup', function() {
	// Do anything after initialization but before
});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function() {
	game.socket.on('module.tde5e', (data) => {socket.messageReceived(data)})
});

/* -------------------------------------------- */
/*  Other Hooks                                 */
/* -------------------------------------------- */

Hooks.on("createActor", initialActorLoadListener);
Hooks.on("renderChatMessage", chat.handleChatMessageRender);
Hooks.on("updateChatMessage", chat.handleChatMessageUpdate)


// Hooks.on("createChatMessage", (a, b, c) =>
// { 
// 	JSON.parse(JSON.stringify(a || {}));
// 	if (a.data.content.startsWith("!TEST"))
// 	{
// 		if (a.user.isSelf)
// 		{
// 		}
// 	} 
// });


